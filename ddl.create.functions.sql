-- DDL.Create Functions

-- 1.Create a view called "sales_revenue_by_category_qtr" that shows the film category and total sales revenue for the current quarter. The view should only display categories with at least one sale in the current quarter. The current quarter should be determined dynamically.

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr
()
RETURNS TABLE
(category_name TEXT, total_revenue NUMERIC) AS $$
BEGIN
    RETURN QUERY
    WITH
        CurrentQuarter
        AS
        (
            SELECT
                EXTRACT(YEAR FROM CURRENT_DATE) AS current_year,
                EXTRACT(QUARTER FROM CURRENT_DATE) AS current_quarter
        ),
        QuarterSales
        AS
        (
            SELECT
                fc.category_id,
                c.name AS category_name,
                SUM(p.amount) AS total_revenue
            FROM
                payment p
                JOIN
                rental r ON p.rental_id = r.rental_id
                JOIN
                inventory i ON r.inventory_id = i.inventory_id
                JOIN
                film_category fc ON i.film_id = fc.film_id
                JOIN
                category c ON fc.category_id = c.category_id
                JOIN
                CurrentQuarter cq ON TRUE
            WHERE
            EXTRACT(YEAR FROM p.payment_date) = cq.current_year
                AND EXTRACT(QUARTER FROM p.payment_date) = cq.current_quarter
            GROUP BY
            fc.category_id, c.name
            HAVING
            SUM(p.amount) > 0
        )
    SELECT
        qs.category_name,
        qs.total_revenue
    FROM
        QuarterSales qs
    ORDER BY
        qs.total_revenue DESC;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT *
FROM get_sales_revenue_by_category_qtr();


WITH
    CurrentQuarter
    AS
    (
        SELECT
            EXTRACT(YEAR FROM CURRENT_DATE) AS current_year,
            EXTRACT(QUARTER FROM CURRENT_DATE) AS current_quarter
    )
SELECT
    p.payment_date,
    p.amount
FROM
    payment p
    JOIN
    CurrentQuarter cq ON TRUE
WHERE 
    EXTRACT(YEAR FROM p.payment_date) = cq.current_year
    AND EXTRACT(QUARTER FROM p.payment_date) = cq.current_quarter;

WITH
    CurrentQuarter
    AS
    (
        SELECT
            EXTRACT(YEAR FROM CURRENT_DATE) AS current_year,
            EXTRACT(QUARTER FROM CURRENT_DATE) AS current_quarter
    )
SELECT
    c.name AS category_name,
    SUM(p.amount) AS total_revenue
FROM
    payment p
    JOIN
    rental r ON p.rental_id = r.rental_id
    JOIN
    inventory i ON r.inventory_id = i.inventory_id
    JOIN
    film_category fc ON i.film_id = fc.film_id
    JOIN
    category c ON fc.category_id = c.category_id
    JOIN
    CurrentQuarter cq ON TRUE
WHERE
    EXTRACT(YEAR FROM p.payment_date) = cq.current_year
    AND EXTRACT(QUARTER FROM p.payment_date) = cq.current_quarter
GROUP BY
    c.name
HAVING
    SUM(p.amount) > 0
ORDER BY
    total_revenue DESC;

-- 2.Create a query language function called "get_sales_revenue_by_category_qtr" that accepts one parameter representing the current quarter and returns the same result as the "sales_revenue_by_category_qtr" view.

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr
(current_quarter TEXT)
RETURNS TABLE
(category_name TEXT, total_revenue NUMERIC) AS $$
DECLARE
    start_date DATE;
    end_date DATE;
BEGIN

    IF current_quarter = 'Q1' THEN
        start_date := DATE_TRUNC
    ('year', CURRENT_DATE);
end_date := start_date + INTERVAL '3 months' - INTERVAL '1 day';
    ELSIF current_quarter = 'Q2' THEN
        start_date := DATE_TRUNC
('year', CURRENT_DATE) + INTERVAL '3 months';
        end_date := start_date + INTERVAL '3 months' - INTERVAL '1 day';
    ELSIF current_quarter = 'Q3' THEN
        start_date := DATE_TRUNC
('year', CURRENT_DATE) + INTERVAL '6 months';
        end_date := start_date + INTERVAL '3 months' - INTERVAL '1 day';
    ELSIF current_quarter = 'Q4' THEN
        start_date := DATE_TRUNC
('year', CURRENT_DATE) + INTERVAL '9 months';
        end_date := start_date + INTERVAL '3 months' - INTERVAL '1 day';
    ELSE
        RAISE EXCEPTION 'Invalid quarter: %', current_quarter;
END
IF;

    RETURN QUERY
WITH
    QuarterSales
    AS
    (
        SELECT
            c.name AS category_name,
            SUM(p.amount) AS total_revenue
        FROM
            payment p
            JOIN
            rental r ON p.rental_id = r.rental_id
            JOIN
            inventory i ON r.inventory_id = i.inventory_id
            JOIN
            film_category fc ON i.film_id = fc.film_id
            JOIN
            category c ON fc.category_id = c.category_id
        WHERE
            p.payment_date BETWEEN start_date AND end_date
        GROUP BY
            c.name
        HAVING
            SUM(p.amount) > 0
    )
SELECT
    qs.category_name,
    qs.total_revenue
FROM
    QuarterSales qs
ORDER BY
        qs.total_revenue DESC;
END;
$$ LANGUAGE plpgsql;

-- 3.Create a procedure language function called "new_movie" that takes a movie title as a parameter and inserts a new movie with the given title in the film table. The function should generate a new unique film ID, set the rental rate to 4.99, the rental duration to three days, the replacement cost to 19.99, the release year to the current year, and "language" as Klingon. The function should also verify that the language exists in the "language" table. Then, ensure that no such function has been created before; if so, replace it.

INSERT INTO language
    (name)
VALUES
    ('Klingon');

CREATE OR REPLACE FUNCTION new_movie
(movie_title TEXT)
RETURNS VOID AS $$
DECLARE
    klingon_language_id INTEGER;
    current_year INTEGER;
BEGIN
 
    current_year := EXTRACT
(YEAR FROM CURRENT_DATE);


SELECT language_id
INTO klingon_language_id
FROM language
WHERE name = 'Klingon';


IF klingon_language_id IS NULL THEN
        RAISE EXCEPTION 'Language "Klingon" does not exist in the language table.';
END
IF;

   
    INSERT INTO film
    (
    title,
    description,
    release_year,
    language_id,
    rental_duration,
    rental_rate,
    length,
    replacement_cost,
    rating,
    special_features
    )
VALUES
    (
        movie_title,
        NULL,
        current_year,
        7,
        3,
        4.99,
        NULL,
        19.99,
        'G',
        NULL                  
    );
END;
$$ LANGUAGE plpgsql;


